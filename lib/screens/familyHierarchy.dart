import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_picker/image_picker.dart';

class FamilyHierarchy extends StatefulWidget {
  @override
  _FamilyHierarchyState createState() => _FamilyHierarchyState();
}

class _FamilyHierarchyState extends State<FamilyHierarchy> {
  bool showAddMember = false;
  PickedFile _imagePick;
  final _picker = ImagePicker();

  void picPhoto(ImageSource source) async {
    final picedFile = await _picker.getImage(source: source);
    setState(() {
      _imagePick = picedFile;
    });
  }

  String relationship;
  List listitems = ['Mother', 'Father', 'Brother', 'Sister', 'Uncle', 'Aunt','Wife','Husband'];

  DateTime _date = DateTime.now();
  Future<Null> selectDate(BuildContext context) async {
    DateTime _datePicker = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: DateTime(1947),
        lastDate: DateTime(2050));
    if (_datePicker != null && _datePicker != _date) {
      setState(() {
        _date = _datePicker;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Family Hierarchy',
          style: TextStyle(color: Colors.white),
        ),
        backgroundColor: Color(0xFFA96469),
      ),
      body: SingleChildScrollView(child: profileView()),
    );
  }

  Widget profileView() {
    return Center(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 38.0),
            child: Center(
              child: CircleAvatar(
                  radius: 50,
                  backgroundImage: AssetImage('assets/profile.jpeg')),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              'Adam Besaw',
              style: TextStyle(color: Colors.black, fontSize: 20),
            ),
          ),
          ProfileFields(
            text1: 'First Name',
            text1A: 'Adam',
            text2: '  Last Name',
            text2A: 'Besaw',
          ),
          ProfileFields(
            text1: 'Date of Birth',
            text1A: '05/07/1996',
            text2: 'Anniversart Date',
            text2A: '09/01/2018',
          ),
          Padding(
            padding: const EdgeInsets.only(top: 18.0, left: 20),
            child: Row(
              children: <Widget>[
                Column(
                  children: [
                    Text('Phone Number', style: TextStyle(color: Colors.grey)),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text('+374 12345678',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    )
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 18.0, left: 20),
            child: Row(
              children: <Widget>[
                Column(
                  children: [
                    Text('Eircode', style: TextStyle(color: Colors.grey)),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text('7777777',
                          style: TextStyle(fontWeight: FontWeight.bold)),
                    )
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          SizedBox(
            height: 20,
            width: 300,
            child: Divider(
              color: Color(0xFFA96469),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 8.0, left: 20),
            child: Row(
              children: [
                Text(
                  'Family Members',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                ),
                SizedBox(
                  width: 80,
                ),
                Container(
                  child: TextButton(
                    style: TextButton.styleFrom(
                      backgroundColor: Color(0xFFA96469),
                    ),
                    onPressed: () {
                      setState(() {
                        showAddMember = true;
                      });
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8),
                      child: Text(
                        'Add Member',
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          showAddMember == true
              ? Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Container(
                      height: 480,
                      width: 500,
                      decoration: BoxDecoration(
                          color: Color(0xFFe2cbcd),
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: addMember(),
                    ),
                  ),
                )
              : Text('')
        ],
      ),
    );
  }

  Widget addMember() {
    return Column(
      children: [
        Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
            child: TextField(
              keyboardType: TextInputType.emailAddress,
              style: TextStyle(fontSize: 14),
              decoration: InputDecoration(
                labelText: 'Email id',
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0),
          child: Row(
            children: [
              Text(
                'Type Of Relationship',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(
                width: 110,
              ),
              DropdownButton(
                  hint: Text('Select'),
                  value: relationship,
                  onChanged: (newValue) {
                    setState(() {
                      relationship = newValue;
                    });
                  },
                  items: listitems.map((valueItem) {
                    return DropdownMenuItem(
                        value: valueItem, child: Text(valueItem));
                  }).toList())
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 18.0, top: 10),
          child: Column(
            children: <Widget>[
              Column(
                children: <Widget>[
                  imagepick(),

                  // TextField()
                ],
              ),
            ],
          ),
        ),
        PtextField(
          labelText: 'Date of Birth',
          hintText: 'MM/DD/YYYY',
        ),
        PtextField(
          labelText: 'Anniversary Date',
          hintText: 'MM/DD/YYYY',
        ),
        PtextField(
          labelText: 'Phone Number',
        ),
        Row(
          children: [
            Pbutton(
              text: 'Save',
              coloar: 0xFFA96469,
            ),
            SizedBox(width: 10),
            Pbutton(
              text: 'Delete',
              coloar: 0xFF808080,
            ),
          ],
        ),
      ],
    );
  }

  Widget bottomBanner() {
    return Container(
      height: 100.0,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(
        children: <Widget>[
          Text(
            'Choose profile Photo',
            style: TextStyle(fontSize: 20.0),
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: <Widget>[
              // ignore: deprecated_member_use
              FlatButton.icon(
                  onPressed: () {
                    picPhoto(ImageSource.camera);
                  },
                  icon: Icon(Icons.camera),
                  label: Text('Camera')),
              // ignore: deprecated_member_use
              FlatButton.icon(
                  onPressed: () {
                    picPhoto(ImageSource.gallery);
                  },
                  icon: Icon(Icons.camera),
                  label: Text('Gallery'))
            ],
          )
        ],
      ),
    );
  }

  Widget imagepick() {
    return Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.topLeft,
          child: GestureDetector(
            onTap: () {
              showModalBottomSheet(
                  context: context, builder: ((builder) => bottomBanner()));
            },
            child: Padding(
              padding: const EdgeInsets.only(top: 18.0),
              child: CircleAvatar(
                  radius: 40,
                  backgroundColor: Colors.grey,
                  backgroundImage: _imagePick == null
                      ? NetworkImage(
                          'https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_640.png')
                      : FileImage(File(_imagePick.path))),
            ),
          ),
        ),
        // SizedBox(width: 40,),
        Container(
          margin: EdgeInsets.only(left: 110, right: 20),
          child: TextField(
            keyboardType: TextInputType.name,
            style: TextStyle(fontSize: 14),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.only(top: 10.0),
              labelText: 'First Name',
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 40, left: 110, right: 20),
          child: TextField(
            keyboardType: TextInputType.name,
            style: TextStyle(fontSize: 14),
            decoration: InputDecoration(
              // contentPadding: ,
              labelText: 'Last Name',
            ),
          ),
        ),
      ],
    );
  }
}

class PtextField extends StatelessWidget {
  final String labelText;
  final String hintText;
  final String type;

  PtextField({
    Key key,
    this.labelText,
    this.hintText,
    this.type,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 5),
      child: Container(
        child: TextField(
          keyboardType: TextInputType.datetime,
          onTap: () {},
          style: TextStyle(fontSize: 14),
          decoration: InputDecoration(
            contentPadding: EdgeInsets.only(top: 10.0),
            labelText: labelText,
            hintText: hintText,
          ),
        ),
      ),
    );
  }
}

class Pbutton extends StatelessWidget {
  final String text;
  final int coloar;
  Function onPressed;

  Pbutton({
    Key key,
    this.text,
    this.coloar,
    this.onPressed,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 50, top: 20),
      child: Container(
        child: TextButton(
          style: TextButton.styleFrom(
            backgroundColor: Color(coloar),
          ),
          onPressed: () {
            onPressed();
          },
          child: Padding(
            padding: const EdgeInsets.only(left: 28.0, right: 28),
            child: Text(
              text,
              style: TextStyle(color: Colors.white, fontSize: 18),
            ),
          ),
        ),
      ),
    );
  }
}

class ProfileFields extends StatelessWidget {
  final String text1;
  final String text1A;
  final String text2;
  final String text2A;

  ProfileFields({
    Key key,
    this.text1,
    this.text1A,
    this.text2,
    this.text2A,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 18.0, left: 20),
      child: Row(
        children: <Widget>[
          Column(
            children: [
              Text(text1, style: TextStyle(color: Colors.grey)),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child:
                    Text(text1A, style: TextStyle(fontWeight: FontWeight.bold)),
              )
            ],
          ),
          SizedBox(
            width: 150,
          ),
          Column(
            children: [
              Text(text2, style: TextStyle(color: Colors.grey)),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child:
                    Text(text2A, style: TextStyle(fontWeight: FontWeight.bold)),
              )
            ],
          ),
        ],
      ),
    );
  }
}
